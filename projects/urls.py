from django.urls import path
from projects.views import (
    list_projects,
    task_list,
    create_project,
)

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", task_list, name="show_project"),
    path("create/", create_project, name="create_project"),
]
