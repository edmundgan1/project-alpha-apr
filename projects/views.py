from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from tasks.models import Task
from .forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": project,
    }
    return render(request, "projects/list.html", context)


@login_required
def task_list(request, id):
    project = Project.objects.get(id=id)
    task = Task.objects.all()
    context = {
        "project_list": project,
        "task_list": task,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
        else:
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
